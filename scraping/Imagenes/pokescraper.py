import requests
import lxml.html as html
import os
import datetime

HOME_URL = ['https://www.pokemonpets.com/Pokedex','https://www.pokemonpets.com/Pokedex-2',
            'https://www.pokemonpets.com/Pokedex-3','https://www.pokemonpets.com/Pokedex-4',
            'https://www.pokemonpets.com/Pokedex-5','https://www.pokemonpets.com/Pokedex-6',
            'https://www.pokemonpets.com/Pokedex-7','https://www.pokemonpets.com/Pokedex-8',
            'https://www.pokemonpets.com/Pokedex-9']

XPATH_LINK_TO_POKEMON = '//td[@class="ML2"]/a/@href'
XPATH_LINK_TO_IMAGE = '//div[@class="MonsterImageBG"]/img/@src'


def pokeimage(): #Función para hacer webscraping de las imagenes
    url_image = parse_image()
    
    for i in range(0,len(url_image)):
        
        name_image = 'Imagenes_Pokedex/Pokemon_'+ str(i+1) +'.png' #nombramos cada imagen
        print(name_image)
        
        url = url_image[i]
        myfile = requests.get(url)

        image = open(name_image,'wb')
        image.write(myfile.content)
        image.close()
        
def parse_image(): #Función para hacer webscraping a cada pokemon y obtener url de imagen
    pokedex = parse_pokemon()
    
    poke_image = []

    for i in range(0,len(pokedex)):
        
        try:
            response = requests.get(pokedex[i])
            if response.status_code == 200:
                image = response.content.decode('utf-8')
                parsed = html.fromstring(image)
                links_to_image = parsed.xpath(XPATH_LINK_TO_IMAGE)
                for j in range (1):
                    poke_image.append('https:'+links_to_image[j])
                
                print(poke_image)

            else:
                raise ValueError(f'Error: {response.status_code}')
        except ValueError as ve:
            print(ve)
    return poke_image
            


def parse_pokemon(): #Función para hacer webscraping a pokedex y obtener url de cada pokemon
    pokelink = []
    for i in range(0,len(HOME_URL)):
        try:
            response = requests.get(HOME_URL[i])
            if response.status_code == 200:
                home = response.content.decode('utf-8')
                parsed = html.fromstring(home)
                links_to_pokemon = parsed.xpath(XPATH_LINK_TO_POKEMON)
                for j in range(100):
                    pokelink.append('https://www.pokemonpets.com/'+links_to_pokemon[j])
                    print(pokelink)
                  
            else:
                raise ValueError(f'Error: {response.status_code}')
        except ValueError as ve:
            print(ve)
    return pokelink

def run():
    pokeimage()


if __name__ == '__main__':
    run()