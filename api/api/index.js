const agent = require('@google-cloud/debug-agent');

const express = require('express')
const bodyParser = require('body-parser')
const helmet = require("helmet");
const { api, dev } = require('../config')

const pokemon = require('./components/pokemon/network')
const curiosities = require('./components/curiosities/network')

const cors = require('cors')
const swaggerUi = require('swagger-ui-express')

const {
    logErrors,
    wrapErrors,
    errorHandler
} = require('../utils/middleware/errorHandlers.js');
const notFoundHandler = require('../utils/middleware/notFoundHandler');
const swaggerDoc = require('./swagger.json')

if(!dev){
    agent.start();
}

const app = express()

app.use(cors());
app.use(bodyParser.json());
app.use(helmet());

// Routing
app.use('/api/pokemon/', pokemon);
app.use('/api/curiosities/', curiosities)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))
app.get('/dast-website-target', function (req, res) {
    res.append('Gitlab-DAST-Permission', 'allow')
    res.send('Respond to DAST ping')
})

// Catch 404
app.use(notFoundHandler);

// Errors middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);


app.listen(api.port, () => {
    console.log(`API escuchando en el puerto ${api.port}`);
})