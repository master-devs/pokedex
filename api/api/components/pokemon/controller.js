//const mocks = require('../../../utils/mocks/pokemon')
const collection = 'pokemon';

module.exports = function (injectedStore) {
  let store = injectedStore;

  if (!store) {    
    store = require('../../../store/dummy');
  }

  function list({ page, limit, type, name }) {
    let query = { type, name };
    return store.list({collection, page, limit, query });
  }

  function get({ pokemonId }) {
    return store.get(collection, pokemonId);
  }

  return {
    list,
    get,
  };
};
