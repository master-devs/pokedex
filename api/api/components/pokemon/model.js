  
const mongoose = require('mongoose');

const { Schema } = mongoose;

const pokemonSchema = new Schema({
    number: Number,
    name: String,
    type1: String,
    type2: String,
    classification: String,
    generation: Number,
    hp: Number,
    height_m: Number,
    weight_kg: Number,
    attack: Number,
    defense: Number,
    sp_attack: Number,
    sp_defense: Number,
    speed: Number,
    abilities: String,
    against_bug: Number,
    against_dark: Number,
    against_dragon: Number,
    against_electric: Number,
    against_fairy: Number,
    against_fight: Number,
    against_fire: Number,
    against_flying: Number,
    against_ghost: Number,
    is_legendary: Number,
    evolution_line: String,
    description: String,
    image_link: String,
});

const modelPokemon = mongoose.model('pokemon', pokemonSchema);
module.exports = modelPokemon;