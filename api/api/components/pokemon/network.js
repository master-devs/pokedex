const express = require('express');
const router = express.Router();
const Controller = require('./index');

const validationHandler = require('../../../utils/middleware/validationHandler');
const { pokemonIdSchema } = require('../../../utils/schemas/pokemon');
const response = require('../../../network/response');

router.get('/', list);
router.get(
  '/:pokemonId',
  validationHandler({ pokemonId: pokemonIdSchema }, 'params'),
  get
);

async function list(req, res, next) {
  try {
    let { page = 1, limit = 20, type = '', name = '' } = req.query;

    const pokemons = await Controller.list({ page, limit, type, name });
    response.success(req, res, pokemons, 200);
  } catch (err) {
    next(err);
  }
}

async function get(req, res, next) {
  try {
    const { pokemonId } = req.params;
    const pokemon = await Controller.get({ pokemonId });
    response.success(req, res, pokemon, 200);
  } catch (err) {
    next(err);
  }
}

module.exports = router;
