const ControllerPokemon = require('../pokemon/index');

module.exports = function (injectedStore) {
  let store = injectedStore;

  if (!store) {
    store = require('../../../store/dummy');
  }

  async function get({ typeCuriositie, page, limit }) {
    const response = await store.get({ typeCuriositie });

    if (!response.data.body.type) {
        return Promise.reject(new Error('Error'));
    }

    let type = response.data.body.type;

    const listPokemon = await ControllerPokemon.list({ page, limit, type });

    const resRequest = {};
    resRequest.type = response.data.body.type;
    resRequest.pokemon = listPokemon;

    return Promise.resolve(resRequest);
  }

  return {
    get,
  };
};
