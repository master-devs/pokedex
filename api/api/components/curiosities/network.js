const express = require('express');
const router = express.Router();
const Controller = require('./index');

const validationHandler = require('../../../utils/middleware/validationHandler');
const { typeCuriositieSechema } = require('../../../utils/schemas/pokemon');
const response = require('../../../network/response');

router.get(
  '/:typeCuriositie',
  validationHandler({ typeCuriositie: typeCuriositieSechema.required() }, 'params'),
  get
);


async function get(req, res, next) {
  try {
    let { typeCuriositie } = req.params;
    let { page = 1, limit = 5} = req.query;
    const data = await Controller.get({ typeCuriositie,page,limit });
    response.success(req, res, data, 200);
  } catch (err) {
    next(err);
  }
}

module.exports = router;
