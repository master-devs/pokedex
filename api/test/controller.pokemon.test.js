const pokemonMock = require('../utils/mocks/pokemonMokc');
const controller = require('../api/components/pokemon/controller');
const store = require('../store/dummy');

const Controller = controller(store);

describe('controller - pokemon', () => {
  test('should return list of exam', () => {
    return Controller.list({page: 1, limit: 20}).then((pokemon) => {
      expect(pokemon).toStrictEqual(pokemonMock);
    });
  });
});