const testServer = require('../utils/testServer');

describe('routes - pokemon', function () {
    const route = require('../api/components/pokemon/network')

    const request = testServer(route);
    describe('GET /pokemon', function () {
        test('should respond with status 200', function (done) {
            request.get('/').expect(200, done);
        });

        test('should respond with status 200', function (done) {
            request.get('/?page=3').expect(200, done);
        });

        test('should respond with status 200', function (done) {
            request.get('/?page=3&limit=10&').expect(200, done);
        });

        test('should respond with status 200', function (done) {
            request.get('/?page=3&limit=10&type=grass').expect(200, done);
        });

        test('should respond with status 200', function (done) {
            request.get('/100').expect(200, done);
        });
    });
});

