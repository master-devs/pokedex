const pokemonMock = require('./pokemonMokc')

const listPokemon = ({ page }) => {
    return new Promise((resolve, reject) => {
        let maxPage = pokemonMock.length / 20;
        if (page > maxPage) {
            reject(new Error('Max page is 2'))
        }
        let end = page * 20;
        let start = end - 20;
        let listPokemon = pokemonMock.slice(start, end)
        resolve(listPokemon)
    })
}

const getPokemon = ({ pokemonId }) => {
    return new Promise((resolve, reject) => {
        let pokemonDetails = pokemonMock.find(element => element.pokedex_number == pokemonId);

        if (!pokemonDetails) {
            reject(new Error('Not found pokemon'))
        }
        resolve(pokemonDetails)
    })
}

module.exports = {
    listPokemon,
    getPokemon,
}