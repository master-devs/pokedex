const joi = require('@hapi/joi');

const pokemonIdSchema = joi.number();
const typeCuriositieSechema = joi.string().valid(...['weakest','strongest','legendary']);

module.exports = {
    pokemonIdSchema,
    typeCuriositieSechema
}