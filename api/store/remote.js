const axios = require('axios').default

async function get({ typeCuriositie }) {
  return axios.get(`https://platzidex-nestor.ue.r.appspot.com/curiosities/${typeCuriositie}`)
}

module.exports = {
  get,
};
