const mongodb = require('mongoose');
const { db } = require('../config');

const PokemonModel = require('../api/components/pokemon/model');

mongodb.Promise = global.Promise;
const url = `mongodb+srv://${db.dbUser}:${db.dbPassword}@${db.dbHost}/${db.dbName}`;

mongodb.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

console.info('[DB] Connection successfully');

async function list({ collection, page, limit, query }) {
  if (collection === 'pokemon') {
    let querPokemon = {};
    const { type, name } = query;

    if (type) {
      querPokemon.$or = [{ type1: type }, { type2: type }];
    }

    if (name) {
      querPokemon.name = { $regex: '.*' + name + '.*', $options: 'gi' };
    }
    const pokemons = PokemonModel.find(querPokemon)
      .limit(limit * 1)
      .skip((page - 1) * limit);
    return pokemons;
  }

  return null;
}

async function get(collection, id) {
  if (collection === 'pokemon') {
    const pokemon = PokemonModel.findOne({ number: id });
    return pokemon;
  }

  return null;
}

module.exports = {
  list,
  get,
};
