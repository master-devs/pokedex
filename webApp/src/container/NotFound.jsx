import React from 'react';
import Header from '../components/Header.jsx';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <>
      <Header />
      <span className='margin-desktop HideOnMobile' />
      <span className='margin-desktop HideOnMobile' />
      <div className='wrapper-itemsHome'>
        <h1>404 Not found</h1>
        <p>
          The page you are looking from it's not here, try to do another search.
        </p>
        <button type='button' aria-label='back' className='btn-notFound'>
          <Link to='/'>Go home</Link>
        </button>
      </div>
    </>
  );
};

export default NotFound;
