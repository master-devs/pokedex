import React from 'react';
import Header from '../components/Header.jsx';
import HeaderMobile from '../components/HeaderMobile.jsx';
import Filters from '../components/Filters.jsx';

import Strongest from '../components/Strongest.jsx';
import Weakest from '../components/Weakest.jsx';
import MaybeLegendary from '../components/MaybeLegendary.jsx';

const Curiosities = () => {
  return (
    <>
      <Header />
      <HeaderMobile />
      <span className='margin ShowOnMobile' />
      <span className='margin-desktop HideOnMobile' />
      <Filters />
      <div className='curiosities'>
        <div className='wrapper-itemsHome curiosities--title'>
          <h1>Curiosities</h1>
          <p>
            This is a list of Pokémon in the order dictated by the National
            Pokédex, meaning that Pokémon from the Kanto region will appear
            first, followed by those from Johto, Hoenn, Sinnoh, Unova, Kalos,
            Alola, and Galar.
          </p>
        </div>
        <div className='wrapper-itemsHome'>
          <h2>Top strongest Pokemons</h2>
        </div>
        <main className='wrapper-itemsHome'>
          <Strongest />
        </main>
        <div className='wrapper-itemsHome'>
          <h2>Top weakest Pokemons</h2>
        </div>
        <main className='wrapper-itemsHome'>
          <Weakest />
        </main>
        <div className='wrapper-itemsHome'>
          <h2>Top Pokemons that could be Legendary</h2>
        </div>
        <main className='wrapper-itemsHome'>
          <MaybeLegendary />
        </main>
      </div>
      <span className='margin-desktop HideOnMobile' />
    </>
  );
};

export default Curiosities;
